Dodaj indeksy

* order_products_part 
  * indeks `brin` na pole `order_id`
* orders_part
  * index `btree` na pola `id` `date` `price`
order_products_part 

```postgresql
CREATE INDEX idx_order_products_parts_order_id
    ON public.order_products_part USING brin
    (order_id)
;
```
```postgresql
CREATE INDEX idx_order_products_parts_order_id_date_price
    ON public.order_products_part USING btree
        (order_id ASC NULLS LAST, date ASC NULLS LAST, price ASC NULLS LAST)
;
```
```postgresql
CREATE INDEX idx_orders_part_date
    ON public.orders_part USING btree
    (add_date ASC NULLS LAST)
;
```

```postgresql
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT *,
       (SELECT SUM(price)
        FROM order_products_parts
        WHERE o.id = order_id
          AND date >= '2020-11-01'
          AND date < '2020-12-01')
FROM orders_parts o
         INNER JOIN order_products_parts ON o.id = order_id AND date > '2020-11-01' AND date < '2020-12-01' AND price > 8000
WHERE o.add_date >= '2020-11-01'
  AND o.add_date < '2020-12-01'
ORDER BY o.id DESC
LIMIT 100
```

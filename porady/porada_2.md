Posprzątaj query

```postgresql
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT *,
       (SELECT SUM(price)
        FROM order_products_parts
        WHERE o.id = order_id
          AND date >= '2020-11-01'
          AND date < '2020-12-01')
FROM orders_parts o
         INNER JOIN order_products_parts ON o.id = order_id AND date > '2020-11-01' AND date < '2020-12-01' AND price > 8000
WHERE o.add_date >= '2020-11-01'
  AND o.add_date < '2020-12-01'
ORDER BY o.id DESC
LIMIT 100
```


* Porównaj explain z przed oraz po operacji.
* Zobacz jak bardzo dodanie warunków z datą zmienia koszt
* Zobacz jak zmiana order by z add_date na id zmienia zapytanie

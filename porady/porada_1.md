Przygotuj partycjonowanie tabeli

```postgresql
-- stworzenie partycjonowanej tabeli dla zamówień
CREATE TABLE "orders_part" (
                               id INTEGER NOT NULL DEFAULT nextval('orders_id_seq'::regclass),
                               name VARCHAR(255),
                               description TEXT,
                               add_date TIMESTAMP,
                               type VARCHAR(60),
                               CONSTRAINT order_part_pkey  PRIMARY KEY (id, add_date)
) PARTITION BY RANGE (add_date);

-- stworzenie partycjonowanej tabeli dla produktów z zamówienia
CREATE TABLE "order_products_part" (
                                       id INTEGER NOT NULL DEFAULT nextval('order_products_id_seq'::regclass),
                                       order_id INTEGER,
                                       name VARCHAR(255),
                                       price NUMERIC,
                                       date TIMESTAMP,
                                       CONSTRAINT order_products_part_pkey  PRIMARY KEY (id, date)
) PARTITION BY RANGE (date);


-- stworzenie partycji domyślnej
CREATE TABLE public.orders_default  PARTITION OF public.orders_part DEFAULT;
CREATE TABLE public.order_products_default  PARTITION OF public.order_products_part DEFAULT;


-- stworzenie partycjonowanej dla wybranego okresu
CREATE TABLE public.orders_y2020m11  PARTITION OF public.orders_part FOR VALUES FROM ('2020-11-01') TO ('2020-12-01');
CREATE TABLE public.order_products_y2020m11  PARTITION OF public.order_products_part FOR VALUES FROM ('2020-11-01') TO ('2020-12-01');

-- skopiowanie danych
INSERT INTO orders_part  (SELECT * FROM  orders);
INSERT INTO order_products_part  (SELECT * FROM  order_products);
```

Sprawdź wynik dla partycjonowanej tabeli
```postgresql
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT 
  *, 
  (SELECT SUM(price) FROM order_products_part WHERE o.id = order_id) 
FROM orders o
INNER JOIN order_products_part p ON o.id = order_id
WHERE o.add_date >= '2020-11-01' AND o.add_date < '2020-12-01' AND p.price > 8000 
ORDER BY o.add_date DESC
LIMIT 100;
```

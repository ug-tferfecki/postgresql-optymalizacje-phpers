Utwórz poniższe funckje

```postgresql
create or replace function order_count(creation_date_beginning TIMESTAMP)
    returns int as
$$
select count(*)
from orders
where add_date = creation_date_beginning;
$$
    language sql;
```

```postgresql
create or replace function order_count(creation_date_beginning TIMESTAMP, creation_date_end TIMESTAMP)
    returns int as
$$
select count(*)
from orders
where add_date >= creation_date_beginning
  and add_date <= creation_date_end;
$$
    language sql;
```



```postgresql
select order_count('2019-12-11');
select order_count('2019-12-11', '2019-12-15');
```

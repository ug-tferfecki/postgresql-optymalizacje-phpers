
# PHPers Kraków #8
## PostgreSQL Optymalizacje


### Środowisko testowe

Uruchom środowisko za pomocą komendy
```
docker compose up -d
```


Skopiuj backup z bazy danych do obrazu postgresql
```
docker cp baza.sql postgres:/baza.sql
```

Przejdź do konsoli kontenera postgres
```
docker exec -it postgres bash
```

Zmień użytkownika
```
su postgres
```

Utwórz bazę do której zostanie odtworzona baza danych
```
createdb testdb
```

Uruchom konsolę konterera i odtwórz bazę
```
pg_restore -d testdb baza.sql
```

Zamknij konsolę

Otwórz w przeglądarce adres z pgAdminem
```
http://localhost:15432/
```

zaloguj się korzystająć z poniższych danych:
```
login: phpers@email.com
hasło: phpers
```

Dodaj nowy serwer
```
Nazwa: phpers
Host: postgres
Port: 5432
Maintenace db: testdb
Username: postgres
Password: phpers
```

W tabelach powinno ci przywrócić 2 tabele:
* orders
* order_products


### Zadanie

Korzystając z poniższego zapytania sróbuj zoptymalizować czas oraz koszt.

```postgresql
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT 
  *, 
  (SELECT SUM(price) FROM order_products WHERE o.id = order_id) 
FROM orders o
INNER JOIN order_products p ON o.id = order_id
WHERE o.add_date >= '2020-11-01' AND o.add_date < '2020-12-01' AND p.price > 8000 
ORDER BY o.add_date DESC
LIMIT 100;
```
https://explain.dalibo.com/plan/9d3040g94dhcad2a

Jeżeli utkniesz spróbuj skorzystać z plików w folderze porady, aby własnoręcznie przeprowadzić optymalizację zapytania
